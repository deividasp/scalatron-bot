import scala.collection.mutable

import scala.util.Random
import scala.util.control.Breaks._

class ControlFunctionFactory {

  def create = {
    ControlFunction.respond _
  }

}

object ControlFunction {

  def respond(input: String) = {
    val (opcode, parameters) = CommandParser(input)

    opcode match {
      case "React" =>
        if (parameters("generation").toInt == 0) {
          controlMaster(new MasterBot(parameters))
        } else {
          controlSlave(new SlaveBot(parameters))
        }
      case _ =>
        ""
    }
  }

  def controlMaster(bot: MasterBot) = {
    val spawnCooldown = bot.getParameter("spawn_cooldown")

    if (!spawnCooldown.isDefined) {
      bot.setParameter("spawn_cooldown", "15")
    } else {
      if (spawnCooldown.get.toInt > 0) {
        bot.setParameter("spawn_cooldown", (spawnCooldown.get.toInt - 1).toString)
      }
    }

    if (spawnCooldown.isDefined && spawnCooldown.get.toInt == 0 && bot.view.countOf(TileContent.SLAVE_BOT) == 0) {
      val direction = bot.view.randomWalkableDirection

      if (direction.isDefined) {
        bot.spawnSlave(direction.get, 100)
        bot.setParameter("spawn_cooldown", "10")
      }
    }

    val goodPlantPosition = bot.view.findNearest(TileContent.GOOD_PLANT)
    val goodBeastPosition = bot.view.findNearest(TileContent.GOOD_BEAST)

    if (goodPlantPosition.isDefined || goodBeastPosition.isDefined) {
      if (goodPlantPosition.isDefined && goodBeastPosition.isDefined) {
        if (bot.view.position.distanceTo(goodPlantPosition.get) < bot.view.position.distanceTo(goodBeastPosition.get)) {
          bot.move(goodPlantPosition.get)
        } else {
          bot.move(goodBeastPosition.get)
        }
      } else if (goodPlantPosition.isDefined) {
        bot.move(goodPlantPosition.get)
      } else {
        bot.move(goodBeastPosition.get)
      }
    } else {
      if (!bot.getParameter("heading_direction").isDefined) {
        val headingDirection = bot.view.randomWalkableDirection

        if (headingDirection.isDefined) {
          bot.setParameter("heading_direction", headingDirection.get.toString)
        }
      } else {
        val offsets = bot.getParameter("heading_direction").get.split(':')

        val offsetX = offsets(0).toInt
        val offsetY = offsets(1).toInt

        if (!bot.move(bot.view.position.offset(offsetX, offsetY))) {
          val headingDirection = bot.view.randomWalkableDirection

          if (headingDirection.isDefined) {
            bot.setParameter("heading_direction", headingDirection.get.toString)
          }
        }
      }
    }

    bot.react
  }

  def controlSlave(bot: SlaveBot) = {
    val badBeastPosition = bot.view.findNearest(TileContent.BAD_BEAST)
    val enemySlaveBotPosition = bot.view.findNearest(TileContent.ENEMY_SLAVE_BOT)

    if (badBeastPosition.isDefined || enemySlaveBotPosition.isDefined) {
      if ((badBeastPosition.isDefined && bot.view.position.distanceTo(badBeastPosition.get) < 2) ||
        (enemySlaveBotPosition.isDefined && bot.view.position.distanceTo(enemySlaveBotPosition.get) < 2)) {
        bot.explode(3)
      }
    }

    bot.react
  }

}

object CommandParser {

  def apply(input: String) = {
    val segments = input.split('(')

    val opcode = segments(0)
    val parameters = segments(1).dropRight(1).split(',')

    val parameterMap = parameters.map(parameter => {
      val segments = parameter.split('=')

      val key = segments(0)
      val value = segments(1)

      (key, value)
    }).toMap

    (opcode, parameterMap)
  }

}

object ViewParser {

  def apply(input: String) = {
    val size = math.sqrt(input.length).toInt
    val centerPosition = Position(size / 2, size / 2)

    val positionMap = input.zipWithIndex.map {
      case (k, v) =>
        (toPosition(v, size), k)
    }.toMap

    val characterMap = input.zipWithIndex.groupBy(_._1).map {
      case (k, v) =>
        (k, v.map(x => toPosition(x._2, size)))
    }

    new View(centerPosition, positionMap, characterMap)
  }

  private def toPosition(index: Int, size: Int) = {
    val x = index % size
    val y = index / size

    Position(x, y)
  }

}

class Bot(parameters: Map[String, String]) {
  private var commands = ""
  private var newParameters = ""

  val view = ViewParser(parameters("view"))

  def appendCommand(command: String) {
    if (!commands.isEmpty) {
      commands += '|'
    }

    commands += command
  }

  def setParameter(key: String, value: String) {
    if (!newParameters.isEmpty) {
      newParameters += ","
    }

    newParameters += key + "=" + value
  }

  def getParameter(key: String): Option[String] = {
    try {
      Some(parameters(key))
    } catch {
      case _: Exception =>
        None
    }
  }

  def move(targetPosition: Position): Boolean = {
    val path = view.findPath(targetPosition)

    if (path.isDefined) {
      move(directionTo(path.get(0)))
      return true
    }

    false
  }

  def spawnSlave(direction: Direction, energy: Int) {
    appendCommand("Spawn(direction=" + direction.toString + ",energy=" + energy + ")")
  }

  def react = {
    if (!newParameters.isEmpty) {
      appendCommand("Set(" + newParameters + ")")
    }

    commands
  }

  private def move(direction: Direction) {
    appendCommand("Move(direction=" + direction.toString + ")")
  }

  private def directionTo(targetPosition: Position) = {
    view.position.directionTo(targetPosition)
  }

}

class MasterBot(parameters: Map[String, String]) extends Bot(parameters) {

}

class SlaveBot(parameters: Map[String, String]) extends Bot(parameters) {

  def masterOffset = parameters("master")

  def explode(size: Int) = {
    appendCommand("Explode(size=" + size + ")")
  }

}

class View(centerPosition: Position, positionMap: Map[Position, Char], characterMap: Map[Char, IndexedSeq[Position]]) {

  private val WALKABLE_TILES = Array(TileContent.MASTER_BOT, TileContent.EMPTY,  TileContent.GOOD_PLANT, TileContent.GOOD_BEAST)

  def randomWalkableDirection: Option[Direction] = {
    val random = new Random
    var direction: Option[Direction] = None

    var possibleDirections = Array(Directions.UP, Directions.DOWN,
                                   Directions.LEFT, Directions.RIGHT,
                                   Directions.LEFT_UP, Directions.LEFT_DOWN,
                                   Directions.RIGHT_UP, Directions.RIGHT_DOWN)

    while (!possibleDirections.isEmpty && !direction.isDefined) {
      val randomIndex = random.nextInt(possibleDirections.size)
      val randomDirection = possibleDirections(randomIndex)
      val position = centerPosition.offset(randomDirection.xOffset, randomDirection.yOffset)

      if (walkable(position)) {
        direction = Some(randomDirection)
      } else {
        possibleDirections = possibleDirections.filterNot(e => e == randomDirection)
      }
    }

    direction
  }

  def countOf(character: Char): Int = {
    if (!characterMap.isDefinedAt(character)) {
      return 0
    }

    characterMap(character).size
  }

  def findNearest(character: Char): Option[Position] = {
    if (!characterMap.isDefinedAt(character)) {
      return None
    }

    val positions = characterMap(character)

    var nearestPosition = positions(0)
    var nearestDistance = centerPosition.distanceTo(positions(0))

    for (position <- positions) {
      if (centerPosition.distanceTo(position) < nearestDistance) {
        nearestPosition = position
        nearestDistance = centerPosition.distanceTo(position)
      }
    }

    Some(nearestPosition)
  }

  def findPath(targetPosition: Position): Option[mutable.MutableList[Position]] = {
    val explorablePositions = mutable.Queue[Position](centerPosition)
    val visitedPositions = mutable.MutableList[Position](centerPosition)
    val previousPositionMap = scala.collection.mutable.Map[Position, Position]()

    var pathFound = false

    while (!explorablePositions.isEmpty) {
      val position = explorablePositions.dequeue

      breakable {
        for (neighborPosition <- walkableNeighbors(position)) {
          if (!visitedPositions.contains(neighborPosition)) {
            visitedPositions += neighborPosition
            previousPositionMap(neighborPosition) = position
            explorablePositions.enqueue(neighborPosition)

            if (neighborPosition == targetPosition) {
              pathFound = true
              break
            }
          }
        }
      }
    }

    if (!pathFound) {
      return None
    }

    Some(buildPath(centerPosition, targetPosition, previousPositionMap))
  }

  def position = {
    centerPosition
  }

  private def buildPath(sourcePosition: Position, targetPosition: Position, previousPositionMap: mutable.Map[Position, Position]) = {
    val path = mutable.MutableList[Position](targetPosition)

    var currentPosition = targetPosition

    breakable {
      while (previousPositionMap.isDefinedAt(currentPosition)) {
        currentPosition = previousPositionMap(currentPosition)

        if (currentPosition == sourcePosition) {
          break
        }

        path += currentPosition
      }
    }

    path.reverse
  }

  private def walkableNeighbors(position: Position) = {
    val neighbors = mutable.MutableList[Position]()

    neighbors += position.offset(0, 1)     //down
    neighbors += position.offset(0, -1)    //up

    neighbors += position.offset(1, 0)     //right
    neighbors += position.offset(-1, 0)    //left

    if (walkable(position.offset(1, 0)) && walkable(position.offset(0, -1))) {
      neighbors += position.offset(1, 1)   //right down
      neighbors += position.offset(1, -1)  //right up
    }

    if (walkable(position.offset(-1, 0)) && walkable(position.offset(0, 1))) {
      neighbors += position.offset(-1, 1)  //left down
      neighbors += position.offset(-1, -1) //left up
    }

    neighbors.filter(walkable)
  }

  private def walkable(position: Position): Boolean = {
    if (!positionMap.isDefinedAt(position)) {
      return false
    }

    WALKABLE_TILES.contains(positionMap(position))
  }

}

case class Position(x: Int, y: Int) {

  def offset(xOffset: Int, yOffset: Int) = {
    Position(x + xOffset, y + yOffset)
  }

  def directionTo(position: Position) = {
    var offsetX = 0
    var offsetY = 0

    if (position.x < x) {
      offsetX = -1
    } else if (position.x > x) {
      offsetX = 1
    }

    if (position.y < y) {
      offsetY = -1
    } else if (position.y > y) {
      offsetY = 1
    }

    Direction(offsetX, offsetY)
  }

  def distanceTo(position: Position) = {
    val deltaX = math.abs(x - position.x)
    val deltaY = math.abs(y - position.y)

    math.sqrt(math.pow(deltaX, 2) + math.pow(deltaY, 2)).toInt
  }

  override def toString = {
    x + ":" + y
  }

}

case class Direction(xOffset: Int, yOffset: Int) {

  override def toString = {
    xOffset + ":" + yOffset
  }

}

object Directions {

  val UP = Direction(0, -1)
  val DOWN = Direction(0, 1)

  val LEFT = Direction(-1, 0)
  val LEFT_UP = Direction(-1, -1)
  val LEFT_DOWN = Direction(-1, 1)

  val RIGHT = Direction(1, 0)
  val RIGHT_UP = Direction(1, -1)
  val RIGHT_DOWN = Direction(1, 1)

}

object TileContent {

  val WALL = 'W'
  val EMPTY = '_'
  val WALL_OCCLUDED = '?'

  val MASTER_BOT = 'M'
  val SLAVE_BOT = 'S'

  val ENEMY_MASTER_BOT = 'm'
  val ENEMY_SLAVE_BOT = 's'

  val GOOD_PLANT = 'P'
  val GOOD_BEAST = 'B'

  val BAD_PLANT = 'p'
  val BAD_BEAST = 'b'

}